<?php

namespace Drupal\Tests\entity_theme_engine\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for serialization.
 *
 * @group serialization
 */
class GenericTest extends GenericModuleTestBase {}
